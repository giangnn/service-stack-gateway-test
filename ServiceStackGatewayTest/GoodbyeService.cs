﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceStackGatewayTest
{
    [Route("/goodbye", "POST")]
    public class Goodbye : IReturn<GoodbyeResponse>
    {
        public string Name { get; set; }
    }

    public class GoodbyeResponse
    {
        public string Result { get; set; }
    }

    public class GoodbyeService : Service
    {
        public GoodbyeResponse Any(Goodbye request)
        {
            return new GoodbyeResponse { Result = "Goodbye, " + request.Name };
        }
    }
}
