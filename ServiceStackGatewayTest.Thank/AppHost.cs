﻿using Funq;
using ServiceStack;
using ServiceStack.Discovery.Redis;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ServiceStackGatewayTest.Thank
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("ServiceStackGatewayTest.Thank", new Assembly[] { typeof(ThankService).Assembly })
        {
        }

        public override void Configure(Container container)
        {
            var redisClientManager = new PooledRedisClientManager("localhost:6379");
            container.AddSingleton<IRedisClientsManager>(redisClientManager);
            SetConfig(new HostConfig
            {
                WebHostUrl = "http://localhost:5001"
            });
            Plugins.Add(new RedisServiceDiscoveryFeature());
        }
    }
}
