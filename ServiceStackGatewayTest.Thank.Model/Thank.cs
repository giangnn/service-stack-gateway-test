﻿using ServiceStack;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceStackGatewayTest.Thank.Model
{
    [Route("/thankyou", "PUT")]
    public class Thankyou : IReturn<ThankyouResponse>
    {
        public string Name { get; set; }
    }

    public class ThankyouResponse
    {
        public string Result { get; set; }
    }
}
