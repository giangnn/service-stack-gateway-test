﻿using Funq;
using ServiceStack;
using ServiceStack.Discovery.Redis;
using ServiceStack.Redis;
using ServiceStack.Web;
using System.Reflection;

namespace ServiceStackGatewayTest
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("ServiceStackGatewayTest", new Assembly[] { typeof(HelloService).Assembly })
        {
        }

        public override void Configure(Container container)
        {
            var redisClientManager = new PooledRedisClientManager("localhost:6379");
            container.AddSingleton<IRedisClientsManager>(redisClientManager);
            SetConfig(new HostConfig
            {
                WebHostUrl = "http://localhost:5000"
            });
            Plugins.Add(new RedisServiceDiscoveryFeature());
        }
    }
}