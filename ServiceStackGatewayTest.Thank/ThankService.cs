﻿using ServiceStack;
using ServiceStackGatewayTest.Thank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceStackGatewayTest.Thank
{   
    public class ThankService : Service
    {
        public ThankyouResponse Any(Thankyou request)
        {
            return new ThankyouResponse { Result = "Thank you, " + request.Name };
        }
    }
}
