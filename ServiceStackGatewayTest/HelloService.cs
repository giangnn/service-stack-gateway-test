﻿using ServiceStack;
using ServiceStack.Web;
using ServiceStackGatewayTest.Thank.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceStackGatewayTest
{
    [Route("/hello/{Name}", "GET")]
    public class Hello : IReturn<HelloResponse>
    {
        public string Name { get; set; }
    }

    public class HelloResponse
    {
        public string Result { get; set; }
    }

    public class HelloService : Service
    {        

        public HelloResponse Any(Hello request)
        {
            var thankyou = new Thankyou { }.PopulateWith(request);
            var thankyouResponse = Gateway.Send(thankyou);

            var goodbye = new Goodbye{ }.PopulateWith(request);
            var goodbyeResponse = Gateway.Send(goodbye);

            return new HelloResponse { Result = $"Hello, {request.Name}. ${thankyouResponse.Result} and ${goodbyeResponse.Result}" };
        }
    }
}
